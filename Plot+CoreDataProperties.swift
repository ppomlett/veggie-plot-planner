//
//  Plot+CoreDataProperties.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 25/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData
//import

extension Plot {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Plot> {
        return NSFetchRequest<Plot>(entityName: "Plot");
    }

    @NSManaged public var plotDescription: String?
    @NSManaged public var name: String?
    @NSManaged public var children: NSSet?
    @NSManaged public var plotNotes: NSSet?
    @NSManaged public var plotToDos: NSSet?

}

// MARK: Generated accessors for children
extension Plot {

    @objc(addChildrenObject:)
    @NSManaged public func addToChildren(_ value: Bed)

    @objc(removeChildrenObject:)
    @NSManaged public func removeFromChildren(_ value: Bed)

    @objc(addChildren:)
    @NSManaged public func addToChildren(_ values: NSSet)

    @objc(removeChildren:)
    @NSManaged public func removeFromChildren(_ values: NSSet)

}

// MARK: Generated accessors for plotNotes
extension Plot {

    @objc(addPlotNotesObject:)
    @NSManaged public func addToPlotNotes(_ value: Note)

    @objc(removePlotNotesObject:)
    @NSManaged public func removeFromPlotNotes(_ value: Note)

    @objc(addPlotNotes:)
    @NSManaged public func addToPlotNotes(_ values: NSSet)

    @objc(removePlotNotes:)
    @NSManaged public func removeFromPlotNotes(_ values: NSSet)

}

// MARK: Generated accessors for plotToDos
extension Plot {

    @objc(addPlotToDosObject:)
    @NSManaged public func addToPlotToDos(_ value: ToDo)

    @objc(removePlotToDosObject:)
    @NSManaged public func removeFromPlotToDos(_ value: ToDo)

    @objc(addPlotToDos:)
    @NSManaged public func addToPlotToDos(_ values: NSSet)

    @objc(removePlotToDos:)
    @NSManaged public func removeFromPlotToDos(_ values: NSSet)

}
