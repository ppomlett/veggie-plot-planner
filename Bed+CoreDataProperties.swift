//
//  Bed+CoreDataProperties.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 31/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData


extension Bed {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Bed> {
        return NSFetchRequest<Bed>(entityName: "Bed");
    }

    @NSManaged public var bedDescription: String?
    @NSManaged public var bedHeight: Float
    @NSManaged public var bedWidth: Float
    @NSManaged public var name: String?
    @NSManaged public var viewX: Float
    @NSManaged public var viewY: Float
    @NSManaged public var viewWidth: Float
    @NSManaged public var viewHeight: Float
    @NSManaged public var bedNotes: NSSet?
    @NSManaged public var bedPlot: Plot?
    @NSManaged public var bedToDos: NSSet?
    @NSManaged public var children: NSSet?

}

// MARK: Generated accessors for bedNotes
extension Bed {

    @objc(addBedNotesObject:)
    @NSManaged public func addToBedNotes(_ value: Note)

    @objc(removeBedNotesObject:)
    @NSManaged public func removeFromBedNotes(_ value: Note)

    @objc(addBedNotes:)
    @NSManaged public func addToBedNotes(_ values: NSSet)

    @objc(removeBedNotes:)
    @NSManaged public func removeFromBedNotes(_ values: NSSet)

}

// MARK: Generated accessors for bedToDos
extension Bed {

    @objc(addBedToDosObject:)
    @NSManaged public func addToBedToDos(_ value: ToDo)

    @objc(removeBedToDosObject:)
    @NSManaged public func removeFromBedToDos(_ value: ToDo)

    @objc(addBedToDos:)
    @NSManaged public func addToBedToDos(_ values: NSSet)

    @objc(removeBedToDos:)
    @NSManaged public func removeFromBedToDos(_ values: NSSet)

}

// MARK: Generated accessors for children
extension Bed {

    @objc(addChildrenObject:)
    @NSManaged public func addToChildren(_ value: Plant)

    @objc(removeChildrenObject:)
    @NSManaged public func removeFromChildren(_ value: Plant)

    @objc(addChildren:)
    @NSManaged public func addToChildren(_ values: NSSet)

    @objc(removeChildren:)
    @NSManaged public func removeFromChildren(_ values: NSSet)

}
