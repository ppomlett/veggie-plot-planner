//
//  Plot+CoreDataClass.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 25/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData


public class Plot: NSManagedObject {
    
    
    public func isLeaf() -> Bool{
        if self.children?.count == 0 {
           return true
        }
        else
        {
            return false
        }
        
    }
    
 
}
