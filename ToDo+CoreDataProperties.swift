//
//  ToDo+CoreDataProperties.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 25/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData
//import

extension ToDo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ToDo> {
        return NSFetchRequest<ToDo>(entityName: "ToDo");
    }

    @NSManaged public var endDate: NSDate?
    @NSManaged public var isDone: Bool
    @NSManaged public var isUser: Bool
    @NSManaged public var startDate: NSDate?
    @NSManaged public var toDoDescription: String?
    @NSManaged public var toDosBed: Bed?
    @NSManaged public var toDosPlant: Plant?
    @NSManaged public var toDosPlot: Plot?

}
