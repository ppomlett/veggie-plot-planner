//
//  SeedBoxItem+CoreDataProperties.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 12/11/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData


extension SeedBoxItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SeedBoxItem> {
        return NSFetchRequest<SeedBoxItem>(entityName: "SeedBoxItem");
    }

    @NSManaged public var distancePlants: Float
    @NSManaged public var distanceRows: Float
    @NSManaged public var harvestFromMonthSection: Int16
    @NSManaged public var imageName: String?
    @NSManaged public var isInBlocks: Bool
    @NSManaged public var isPerennial: Bool
    @NSManaged public var plantFromMonth: Int16
    @NSManaged public var harvestUntillMonthSection: Int16
    @NSManaged public var seedDescription: String?
    @NSManaged public var seedName: String?
    @NSManaged public var plantFromMonthSection: Int16
    @NSManaged public var plantUntillMonthSection: Int16
    @NSManaged public var plantUntillMonth: Int16
    @NSManaged public var harvestFromMonth: Int16
    @NSManaged public var harvestUntillMonth: Int16
    @NSManaged public var isStartIndoors: Bool
    @NSManaged public var startIndoorsWeeks: Int16
    @NSManaged public var family: String?

}
