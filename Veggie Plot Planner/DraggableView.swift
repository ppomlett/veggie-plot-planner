//
//  DraggableView.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 31/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//
// THIS CLASS SHOULD NOT BE CALLED DIRECTLY IT ALLOWS SUBCLASSED VIEWS TO BE MOVED OR RESIZED

import Cocoa

class DraggableView: NSView {

    
    override var isFlipped:Bool {
        get {
            return true
        }
    }
    //Draw Handles
    let handleWidth: CGFloat = 16.0
    let handleHalfWidth: CGFloat = 16.0 / 2.0
    
    func drawHandlesInView (_ view: NSView){
        self.drawHandlesInView(self, point: NSMakePoint(NSMinX(bounds) , NSMinY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMidX(bounds), NSMinY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMaxX(bounds), NSMinY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMinX(bounds), NSMidY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMaxX(bounds), NSMidY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMinX(bounds), NSMaxY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMidX(bounds), NSMaxY(bounds)))
        self.drawHandlesInView(self, point: NSMakePoint(NSMaxX(bounds), NSMaxY(bounds)))
    }
    
    func drawHandlesInView (_ view: NSView, point: NSPoint ){
        var handleBounds: NSRect = NSRect(x: point.x - handleHalfWidth, y: point.y - handleHalfWidth, width: handleWidth, height: handleWidth)
        //set colour and draw handles
        handleBounds = self.centerScanRect(handleBounds)
        var handleShadowBounds  = NSRect(x: point.x - handleHalfWidth - 1, y: point.y - handleHalfWidth - 1, width: handleWidth + 2, height: handleWidth + 2)
        handleShadowBounds = self.centerScanRect(handleShadowBounds)
        //NSOffsetRect(handleBounds, 1.0, 1.0)
        #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).set()
        NSRectFill(handleShadowBounds)
        #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1).set()
        NSRectFill(handleBounds)
    }
    
    //Detect Handles
    func handle(_ position:NSString) -> NSInteger {
        switch position {
        case  "upperleft":
            return 1
        case  "upperMiddle":
            return 2
        case  "upperRight":
            return 3
        case  "middleLeft":
            return 4
        case  "middleRight":
            return 5
        case  "lowerLeft":
            return 6
        case  "lowerMiddle":
            return 7
        case  "lowerRight":
            return 8
        //Should not reach here
        default: return 10
        }
    }
    
    func handleUnderPoint (_ point: NSPoint) -> NSInteger {
        var handle:NSInteger = 0
        if self.isHandleAtPoint(NSMakePoint(NSMinX(bounds), NSMinY(bounds)), underPoint: point) {
            handle = self.handle("upperleft")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMidX(bounds), NSMinY(bounds)), underPoint: point) {
            handle = self.handle( "upperMiddle")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMaxX(bounds), NSMinY(bounds)), underPoint: point) {
            handle = self.handle( "upperRight")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMinX(bounds), NSMidY(bounds)), underPoint: point) {
            handle = self.handle( "middleLeft")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMaxX(bounds), NSMidY(bounds)), underPoint: point) {
            handle = self.handle( "middleRight")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMinX(bounds), NSMaxY(bounds)), underPoint: point) {
            handle = self.handle( "lowerLeft")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMidX(bounds), NSMaxY(bounds)), underPoint: point) {
            handle = self.handle( "lowerMiddle")
        }
        else if self.isHandleAtPoint (NSMakePoint(NSMaxX(bounds), NSMaxY(bounds)), underPoint: point) {
            handle = self.handle( "lowerRight")
        }
        return handle
    }
    
    func isHandleAtPoint (_ handlePoint: NSPoint , underPoint: NSPoint) -> Bool {
        let handleBounds: NSRect = NSRect(x: handlePoint.x - handleHalfWidth, y: handlePoint.y - handleHalfWidth, width: handleWidth, height: handleWidth)
        return NSPointInRect(underPoint, handleBounds)
    }
    
    func resizeByMovingHandle(_ handle: NSInteger, point: NSPoint){
        
        
        // user changing width
        if (handle == self.handle( "upperleft")) || (handle == self.handle( "middleLeft")) || (handle == self.handle( "lowerLeft")) {
            frame.size.width = (framesize?.width)! - point.x
            frame.origin.x = (self.frameOrigin?.x)! + point.x
        }
        else if (handle == self.handle( "upperRight")) || (handle == self.handle( "middleRight")) || (handle == self.handle("lowerRight")) {
            frame.size.width =    (framesize?.width)! + point.x
        }
        // user changing height
        if (handle == self.handle("upperleft")) || (handle == self.handle("upperMiddle")) || (handle == self.handle("upperRight")) {
            frame.size.height = (framesize?.height)! - point.y
            frame.origin.y = (self.frameOrigin?.y)! + point.y
        }
        else if (handle == self.handle( "lowerLeft")) || (handle == self.handle( "lowerMiddle")) || (handle == self.handle( "lowerRight")) {
            frame.size.height =    (framesize?.height)! + point.y
        }
    }
    
    // mouse events
    
    dynamic var viewRect = NSRect()
    var lastMouseLocation: NSPoint?
    var handle: NSInteger?
    var isHandleUnderPoint: Bool?
    var framesize: NSSize?
    var frameOrigin: NSPoint?
   
    
    //SCrollview set by VC so we can find piont in view heirachy
 public var scrollview: NSView?
    
    // notify the VC we have a mouse down on us
 ///   let notificationName = Notification.Name("MouseDownNotification")
    
    override func mouseDown(with event: NSEvent) {
      //  self.isDragged = false
        // self.selected = true
  ///      NotificationCenter.default.post(name: notificationName, object: self)
        // self.viewRect = self.frame
        // mouse down location in window coord system
        self.lastMouseLocation = self.superview?.convert(event.locationInWindow, from: nil)
        // mouse down location in sub view
        // let p = convert( self.lastMouseLocation!, from: self.window?.contentView)
        // mouse down location in scrollView document coord system
        let test = convert( self.lastMouseLocation!, from: self.scrollview)
        self.framesize = frame.size
        self.frameOrigin = frame.origin
        if self.handleUnderPoint(test) != 0 {
            self.handle = self.handleUnderPoint(test)
            self.isHandleUnderPoint = true
        }
        else{
            self.handle = 0
            self.isHandleUnderPoint = false
        }
    }
    
   // var isDragged: Bool?
  ///  let dragNotificationName = Notification.Name("MouseDraggedNotification")
    
    override func mouseDragged(with event: NSEvent) {
   ///     NotificationCenter.default.post(name: dragNotificationName, object: self)
        
//        if isDragged != nil && isDragged == false{
//            // Swift.print("beginUndoGrouping")
//            let undo: UndoManager = undoManager!
//            undo.beginUndoGrouping()
//            isDragged = true
//        }
        let newDragLocation:NSPoint = self.superview!.convert(event.locationInWindow, from: nil)
        var  thisOrigin =  NSPoint(x: NSMinX(frame), y: NSMinY(frame))
        thisOrigin.x += (-self.lastMouseLocation!.x + newDragLocation.x);
        thisOrigin.y += (-self.lastMouseLocation!.y + newDragLocation.y);
        let pointDelta = NSPoint(x: newDragLocation.x - (self.lastMouseLocation?.x)!, y: newDragLocation.y - (self.lastMouseLocation?.y)!)
        if isHandleUnderPoint == true{
            //resize if under handle
            self.resizeByMovingHandle(self.handle!, point: pointDelta)
        }
        else{
            //move the view
            self.setFrameOrigin(thisOrigin)
            self.lastMouseLocation = newDragLocation
        }
        self.viewRect = self.frame
    }
    
    
 ///    let mouseUpNotificationName = Notification.Name("MouseUpNotification")
    override func mouseUp(with event: NSEvent) {
  ///       NotificationCenter.default.post(name: mouseUpNotificationName, object: self.viewRect)
        
//        if isDragged != nil && isDragged == true{
//            let undo: UndoManager = undoManager!
//            undo.endUndoGrouping()
//        }
//        isDragged = false
    }
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        //  Swift.print("callled")
//        if context != &KVOConstant {
//            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
//            return
//        }
//        var oldValue: AnyObject? = change?[NSKeyValueChangeKey.newKey] as AnyObject?
//        if oldValue is NSNull {
//            oldValue = nil
//        }
//        let undo: UndoManager = undoManager!
//        // Swift.print("oldy Value*\(oldValue)")
//        (undo.prepare(withInvocationTarget: object!) as AnyObject).setValue(oldValue, forKeyPath: keyPath!)
//        self.frame = self.viewRect
//        if   undo.isUndoing {
//            undo.setActionName("Move Veg")
//        }
//    }
    
    // View Drawing
    var selected:Bool?
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        self.viewRect = self.frame
//        Swift.print(self.viewRect)
//        self.addObserver(self, forKeyPath:"viewRect", options:.new, context: &KVOConstant)
//        self.selected = false
//    }
    
    override func draw(_ dirtyRect: NSRect) {
       super.draw(dirtyRect) 
        // to be called on double click
          //  #imageLiteral(resourceName: "raisedFrame") .draw(in: self.bounds, from: NSZeroRect, operation: NSCompositingOperation.sourceOver, fraction: 1)
        
        if selected == true {
            self.drawHandlesInView(self)
        }
        
        
    }
    
}
