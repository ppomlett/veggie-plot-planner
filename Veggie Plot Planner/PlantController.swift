//
//  PlantController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 03/11/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.

///*   This class links plant views to plant objects in the model *///

import Cocoa

@objc  protocol PlantControllerDelegate: class {
 @objc optional func reload()
    @objc optional func plantSelected(plant: Plant)
}





class PlantController: NSObject {
   weak var delegate: PlantControllerDelegate?
    let dataHelper = DataHelper.sharedInstance
    var selectedDate: NSDate?
    var bedViewsArray = [BedView]()
    var bedsArray = [Bed]()
    var plantViewsArray = [Array<PlantView>]()
    var plantsArray = [Plant]()
    var seedBoxItem: SeedBoxItem?
    public var selectedView: PlantView?
    var plantViewForBedArray = [PlantView]()
    
    public func selectedSeeBoxItem(seedBoxItem: SeedBoxItem) {
        self.seedBoxItem = seedBoxItem
    }
    
    public func loadPlantViewsAndObjects(parentBed: Bed?, parentView: BedView){
        if (parentBed != nil){
            let parentBed = parentBed! as Bed
            //  self.plantViewsArray = []
            self.plantViewForBedArray = []
            //   self.plantsArray = []
            self.plantsArray = Array(parentBed.children!) as! [Plant]
            print("plantsArray the number of plants on a bed")
            print(self.plantsArray.count)
            for object in self.plantsArray{
                let x = object.viewX
                let y = object.viewY
                let height = object.viewHeight
                let width = object.viewWidth
                let plantView = PlantView(frame:NSRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(width), height: CGFloat(height)))
                let plantDate: Date = object.plantedDate as! Date
                let remDate: Date = object.removedDate as! Date
                let selectedDate: Date = self.selectedDate as! Date
                let fallsBetween = (plantDate...remDate).contains(selectedDate)
                if fallsBetween != true {
                   plantView.alphaValue = 0.2
                    
                }
                
                let isRemoved = (remDate < selectedDate)
                if isRemoved {
                    plantView.isHidden = true
                    
                }
                
               // plantView.alphaValue = 0.3
              //  plantView.hitTest
                plantView.selected = false
                plantView.scrollview = parentView
                parentView.addSubview(plantView)
                let backGroundImage = NSImage(named: object.imageName!)
                let resizedImage = plantView.jresize(backGroundImage!, w: 20, h: 20)
                plantView.backGroundImage(image: resizedImage, width: 50, hieght: 50)
                //  plantView.layer?.backgroundColor = NSColor.green.cgColor
                self.plantViewForBedArray.append(plantView)
            }
            self.plantViewsArray.append(self.plantViewForBedArray)
        }
        print("plantsViewsfor the number of plant views for a perticular bed")
        print(self.plantViewForBedArray.count)
        print("plantsViewsArray should match bedviewsArray empty arrays where beds have no plants")
        print(self.plantViewsArray.count)
        print("bedsArray the number of beds on this plot")
        print(self.bedsArray.count)
        print("bedviewViewsArray represents the number of views for the beds on this plot")
        print(self.bedViewsArray.count)
    }
    
    func createPlantOndrop(_ notf: Notification){
        print("we have a drop")
        let dropView =  notf.object as! BedView
        let dropPoint = dropView.dropPoint
       
        let indexOfDropView = self.bedViewsArray.index(of: dropView)! as Int
        let bedObject = self.bedsArray[indexOfDropView]
     //   var plantsInBedArray = self.plantViewsArray[indexOfDropView]
        let plant = dataHelper.createPlant()
    //    let plantView = PlantView(frame:NSRect(x: (dropPoint?.x)! - 25, y: (dropPoint?.y)! - 25, width: 50, height: 50))
       
     //   plantView.scrollview = dropView
    //    dropView.addSubview(plantView)
       
   //     let backGroundImage = NSImage(named: (self.seedBoxItem?.imageName)!)
   //     let resizedImage = plantView.jresize(backGroundImage!, w: 20, h: 20)
   //     plantView.backGroundImage(image: resizedImage, width: 50, hieght: 50)
   //     plantView.setNeedsDisplay(plantView.bounds)
        
        
        plant.imageName = self.seedBoxItem?.imageName
        plant.viewX =  Float(dropPoint!.x) - 25.0                       // 50.0     //Float(x)
        plant.viewY =  Float(dropPoint!.y) - 25.0     // Float(y)
        plant.viewWidth = 50
        plant.viewHeight = 50
        plant.name = self.seedBoxItem?.seedName
        plant.plantedDate = self.seedBoxItem?.plantOutDate
        plant.removedDate = self.seedBoxItem?.harvestDate
        bedObject.addToChildren(plant)
        
        self.delegate?.reload!()
      //  self.plantsArray.append(plant)
     //   plantsInBedArray.append(plantView)
     //   self.plantViewsArray[indexOfDropView] = plantsInBedArray
    }
    
    func mouseDownOnPlantView(_ notf: Notification){
        let selectedView = notf.object as! PlantView
        self.selectedView = selectedView
        let bedView = self.selectedView?.scrollview
        let indexOfSelectedView = self.bedViewsArray.index(of: bedView as! BedView)! as Int
        let bedObject = self.bedsArray[indexOfSelectedView]
        let plantsInBedArray = self.plantViewsArray[indexOfSelectedView ]
        // self.plantViewsArray = []
        self.plantsArray = Array(bedObject.children!) as! [Plant]
        for plantView in plantsInBedArray{
            if plantView == selectedView{
                plantView.selected = true
                plantView.scrollview = plantView.superview
                plantView.setNeedsDisplay(plantView.bounds)
            }
            else{
                plantView.selected = false
                plantView.setNeedsDisplay(plantView.bounds)
            }
        }
    }
    
    func mouseUpOnPlantView(_ notf: Notification){
        let f = notf.object as! NSRect
        let selectedView = self.selectedView //notf.object as! BedView
        // bedView?.setNeedsDisplay((bedView?.frame)!)
        let X: Float = Float( (f.origin.x))
        let Y: Float = Float( (f.origin.y))
        let width: Float = Float( (f.width))
        let height: Float = Float( (f.height))
        let bedView = self.selectedView?.scrollview
        let indexOfSelectedView = self.bedViewsArray.index(of: bedView as! BedView)! as Int
        let bedObject = self.bedsArray[indexOfSelectedView]
        let plantsInBedArray = self.plantViewsArray[indexOfSelectedView ]
        self.plantsArray = Array(bedObject.children!) as! [Plant]
        let indexOfPlant = plantsInBedArray.index(of: selectedView! as PlantView)! as Int
        let plant = self.plantsArray[indexOfPlant]
        plant.viewX = X
        plant.viewY = Y
        plant.viewWidth = width
        plant.viewHeight = height
        
        //
        self.delegate?.plantSelected!(plant: plant)
    }
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(self.createPlantOndrop), name: NSNotification.Name(rawValue: "seedBoxItemDropped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.mouseDownOnPlantView(_:)), name: NSNotification.Name(rawValue: "MouseDownOnPlantNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.mouseUpOnPlantView(_:)), name: NSNotification.Name(rawValue: "MouseUpOnPlantNotification"), object: nil)
    }
    
    
}
