//
//  PlotView.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 31/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

protocol PlotViewDelegate: class {
    
   func mouseDownOnCanvas(_ sender: PlotView) 
    func processImage(_ image: NSImage, center: NSPoint, onView:PlotView )
    func plotSelected()
}






class PlotView: NSView {

    weak var delegate:PlotViewDelegate?
    
   
    
    var nonURLTypes: Set<String>  { return [String(kUTTypeTIFF)] }
    var acceptableTypes: Set<String> { return nonURLTypes.union([NSURLPboardType]) }
    
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
         self.layer?.backgroundColor = NSColor(patternImage:#imageLiteral(resourceName: "grassTile")).cgColor
        
        
    
      //  self.layer?.masksToBounds = true
        
        // Drawing code here.
    }

   //  override var wantsDefaultClipping:Bool{return false}//avoids clipping the view
    
    override func mouseDown(with event: NSEvent) {
        //  Swift.print("MouseDown main View")
        self.delegate?.mouseDownOnCanvas(self)
        
    }
    
    override func mouseDragged(with event: NSEvent) {
        //  Swift.print("Mousedragged main View")
        
    }
    
    override func mouseUp(with event: NSEvent) {
        self.delegate?.plotSelected()
    }
    
    
    override var isFlipped:Bool {
        get {
            return true
        }
    }
    
    // override var wantsDefaultClipping:Bool{return false}//avoids clipping the view
    
    
    
    let filteringOptions = [NSPasteboardURLReadingContentsConformToTypesKey:NSImage.imageTypes()]
    
    func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
        
        var canAccept = false
        
        //2.
        let pasteBoard = draggingInfo.draggingPasteboard()
        
        //3.
        if pasteBoard.canReadObject(forClasses: [NSURL.self], options: filteringOptions) {
            canAccept = true
        }
            
            
        else if let types = pasteBoard.types , nonURLTypes.intersection(types).count > 0 {
            canAccept = true
        }
        
        
        return canAccept
        
    }
    
    
    var isReceivingDrag = false {
        didSet {
            //  needsDisplay = true
        }
    }
    
    //2.
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        let allow = shouldAllowDrag(sender)
        isReceivingDrag = allow
        
        
        return allow ? .copy : NSDragOperation()
    }
    
    
    override func draggingExited(_ sender: NSDraggingInfo?) {
        
        isReceivingDrag = false
    }
    
    override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let allow = shouldAllowDrag(sender)
        return allow
    }
    
    
    override func performDragOperation(_ draggingInfo: NSDraggingInfo) -> Bool {
        
        //1.
        isReceivingDrag = false
        let pasteBoard = draggingInfo.draggingPasteboard()
        
        //2.
        let point = convert(draggingInfo.draggingLocation(), from: nil)
        //3.
        if let urls = pasteBoard.readObjects(forClasses: [NSURL.self], options:filteringOptions) as? [URL] , urls.count > 0 {
            //  delegate?.processImageURLs(urls, center: point)
            return true
        }
            
        else if let image = NSImage(pasteboard: pasteBoard) {
            delegate?.processImage(image, center: point, onView:self )
            return true
        }
        
        
        
        return false
        
    }
    
    func processImageURLs(_ urls: [URL], center: NSPoint) {
        for (index,url) in urls.enumerated() {
            
            //1.
            if let image = NSImage(contentsOf:url) {
                
                let newCenter = center
                //2.
                if index > 0 {
                    //    newCenter = center.addRandomNoise(Appearance.randomNoise)
                }
                
                //3.
                delegate?.processImage(image, center:newCenter, onView: self)
            }
        }
    }
    
    
    
    
    
    
    
    
    override init(frame frameRect: NSRect) {
        super.init(frame:frameRect);
    }
    // var acceptableTypes: Set<String> { return [NSURLPboardType] }
    
    
    
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //   self.wantsLayer = true
        //  self.layer?.backgroundColor = NSColor.brown().cgColor
        self.register(forDraggedTypes: Array(acceptableTypes))
        
    }
    
 
    
}
