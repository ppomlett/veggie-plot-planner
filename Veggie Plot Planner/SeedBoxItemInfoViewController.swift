//
//  SeedBoxItemInfoViewController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 28/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

protocol SeedBoxItemInfoDelegate: class {
    
    func duplicateSeedBoxItem(item: SeedBoxItem)
    func editSeedBoxItem(item: SeedBoxItem)
    
}


class SeedBoxItemInfoViewController: NSViewController {
    
    let dataHelper = DataHelper.sharedInstance
      var selectedSeedBoxItem: SeedBoxItem!
    weak var delegate: SeedBoxItemInfoDelegate?
    @IBOutlet weak var nameLabel: NSTextField!
    @IBOutlet weak var descriptionLabel: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
      
    override func viewWillAppear() {
        super.viewWillAppear()
        self.nameLabel.stringValue = self.selectedSeedBoxItem.seedName!
        self.descriptionLabel.stringValue = self.selectedSeedBoxItem.seedDescription!
    }
    
    @IBAction func deleteItem(_ sender: NSButton) {
        
        self.dataHelper.deleteSeedBoxItem(seedBoxItem: self.selectedSeedBoxItem)
        self.dismiss(self)
    }
    
    @IBAction func duplicateItem(_ sender: NSButton) {
        self.delegate?.duplicateSeedBoxItem(item: self.selectedSeedBoxItem)
        self.dismiss(self)
    }
    
    @IBAction func editItem(_ sender: NSButton) {
        self.delegate?.editSeedBoxItem(item: self.selectedSeedBoxItem)
        self.dismiss(self)
    }
    
    @IBAction func done(_ sender: NSButton) {
        self.dismiss(self)
    }
    
    
}
