//
//  DraggableViewsController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 02/11/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//  This class links bed views to bed objects in the model and updates the plant controller

import Cocoa
import CoreData


  protocol BedControllerDelegate: class {
    func bedSelected(bed: Bed)
     func plantSelected(plant: Plant)
}



class BedController: NSObject, PlantControllerDelegate {
     weak var delegate: BedControllerDelegate?
    let dataHelper = DataHelper.sharedInstance
    
    var selectedDate: NSDate?
    
    private var bedViewsArray = [BedView]()
    private var bedsArray = [Bed]()
    let plantController = PlantController()
    
    
    func plantSelected(plant: Plant){
       self.delegate?.plantSelected(plant: plant)
        
    }
    
    
    
    //Notification called from a draggable view on mouse down in that view
    public var selectedView: BedView?
    
    func mouseDownOnBedNotification(_ notf: Notification){
        let selectedView = notf.object as! BedView
        self.selectedView = selectedView
        //  let indexOfSelectedView = self.bedViewsArray.index(of: selectedView)! as Int
        //   let bedObject = self.bedsArray[indexOfSelectedView]
        // print(bedObject.debugDescription)
        for draggableView in self.bedViewsArray{
            if draggableView == selectedView{
                draggableView.selected = true
                draggableView.scrollview = draggableView.superview
                draggableView.setNeedsDisplay(draggableView.bounds)
            }
            else{
                draggableView.selected = false
                draggableView.setNeedsDisplay(draggableView.bounds)
            }
        }}
    
    func mouseUpOnBedNotification(_ notf: Notification){
        let f = notf.object as! NSRect
        let bedView = self.selectedView //notf.object as! BedView
        // bedView?.setNeedsDisplay((bedView?.frame)!)
        let X: Float = Float( (f.origin.x))
        let Y: Float = Float( (f.origin.y))
        let width: Float = Float( (f.width))
        let height: Float = Float( (f.height))
        let indexOfA = self.bedViewsArray.index(of: bedView!)! as Int
        let bedObject = self.bedsArray[indexOfA]
        bedObject.viewX = X
        bedObject.viewY = Y
        bedObject.viewWidth = width
        bedObject.viewHeight = height
        self.delegate?.bedSelected(bed: bedObject)
    }
    
    var plotView: NSView?
    var plot: Plot?
    
    public func loadDraggableViewsAndObjects(parentObject: NSManagedObject?, parentView: NSView){
        self.plantController.selectedDate = self.selectedDate
        
        if (parentObject != nil){
            let parentObject = parentObject as! Plot
            self.plot = parentObject
            self.plotView = parentView
            self.bedViewsArray = []
            self.bedsArray = Array(parentObject.children!) as! [Bed]
            self.plantController.plantViewsArray = []
            for object in self.bedsArray{
                let x = object.viewX
                let y = object.viewY
                let height = object.viewHeight
                let width = object.viewWidth
                let bedView = BedView(frame:NSRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(width), height: CGFloat(height)))
                bedView.selected = false
                bedView.scrollview = parentView
                parentView.addSubview(bedView)
                self.bedViewsArray.append(bedView)
                self.plantController.bedsArray = self.bedsArray
                self.plantController.bedViewsArray = self.bedViewsArray
                self.plantController.loadPlantViewsAndObjects(parentBed: object, parentView: bedView)
            }
        }
        self.dataHelper.saveChanges()
    }
    
    public func createDraggableViewAndObject(atPiont: CGPoint, onView: NSView, parentObject: NSManagedObject?){
        let x = atPiont.x - 100
        let y = atPiont.y - 100
        if (parentObject != nil){
            let parentObject = parentObject as! Plot
            let bed = dataHelper.createBed()
         //   let bedView = BedView(frame:NSRect(x: atPiont.x - 100, y: atPiont.y - 100, width: 200, height: 200))
          //  bedView.selected = true
          //  bedView.scrollview = onView
         //   onView.addSubview(bedView)
            bed.viewX = Float(x)
           bed.viewY = Float(y)
            bed.viewWidth = 200
            bed.viewHeight = 200
            bed.name = "New Bed"
            parentObject.addToChildren(bed)
            self.reLoadDraggableViewsAndObjects(parentObject: self.plot, parentView: self.plotView!)
            
            
        //    self.bedsArray.append(bed)
         //   self.bedViewsArray.append(bedView)
            // add enpty array to plantViewsArray to store plants for this bed
          //  self.plantController.plantViewsArray.append([])
        }
     //   self.plantController.bedsArray = self.bedsArray
     //  self.plantController.bedViewsArray = self.bedViewsArray
      //  self.dataHelper.saveChanges()
    }
    
  var parentObject:NSManagedObject?
   var parentView: NSView?
    func reload() {
        
        print("RELOAD")
        self.reLoadDraggableViewsAndObjects(parentObject: parentObject, parentView: parentView!)
    }
    
    
    
    
    
    
    public func reLoadDraggableViewsAndObjects(parentObject:NSManagedObject?, parentView: NSView){
        self.parentObject = parentObject
        self.parentView = parentView
        for draggableView in self.bedViewsArray{
            draggableView.removeFromSuperview()
           
        }
        self.bedViewsArray = []
        self.bedsArray = []
        self.plantController.bedsArray = self.bedsArray
        self.plantController.bedViewsArray = self.bedViewsArray
        self.loadDraggableViewsAndObjects(parentObject: parentObject, parentView: parentView)
    }
    
    public func selectedSeeBoxItem(seedBoxItem: SeedBoxItem){
        self.plantController.selectedSeeBoxItem(seedBoxItem: seedBoxItem)
    }
    
    func undoing(_ notf: Notification){
        //  print("draggable undo")
        self.reLoadDraggableViewsAndObjects(parentObject: parentObject, parentView: parentView!)
    }
    
    func redoing(_ notf: Notification){
        // print(notf.debugDescription)
        self.reLoadDraggableViewsAndObjects(parentObject: parentObject, parentView: parentView!)
    }
    
    override init() {
        super.init()
        self.plantController.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.mouseDownOnBedNotification), name: NSNotification.Name(rawValue: "MouseDownOnBedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.mouseUpOnBedNotification), name: NSNotification.Name(rawValue: "MouseUpOnBedNotification"), object: nil)
        let undomanager = self.dataHelper.managedObjectContext.undoManager
        NotificationCenter.default.addObserver(self, selector: #selector(self.undoing), name: NSNotification.Name.NSUndoManagerDidUndoChange, object: undomanager)
        NotificationCenter.default.addObserver(self, selector: #selector(self.redoing), name: NSNotification.Name.NSUndoManagerDidRedoChange, object: undomanager)
    }
}
