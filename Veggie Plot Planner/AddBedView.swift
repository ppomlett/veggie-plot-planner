//
//  AddBedView.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 31/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa


// MARK: - NSDraggingSource
extension AddBedView: NSDraggingSource {
    //1.
    func draggingSession(_ session: NSDraggingSession, sourceOperationMaskFor context: NSDraggingContext) -> NSDragOperation {
        return .generic
    }
}

// MARK: - NSDraggingSource
extension AddBedView: NSPasteboardItemDataProvider {
    //2.
    func pasteboard(_ pasteboard: NSPasteboard?, item: NSPasteboardItem, provideDataForType type: String) {
        //TODO: Return image data
    }
    
}











class AddBedView: NSView {

    var bedImage: NSImage?
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        let myImage = NSImage(named: "plotBed")
        self.bedImage = myImage
        
       
        
        
        
    }
    
    
    
    
    
    override func mouseDragged(with theEvent: NSEvent) {
        
       
        //1.
        let pasteboardItem = NSPasteboardItem()
        pasteboardItem.setDataProvider(self, forTypes: [kUTTypeTIFF])
        
        
        
        //        let image = self.subviews.first as! NSImageView
      
        
        let tiffdata = self.bedImage?.tiffRepresentation
        pasteboardItem.setData(tiffdata, forType:kUTTypeTIFF as String)
        
        
        
        
        
        
        
        //2.
        let draggingItem = NSDraggingItem(pasteboardWriter: pasteboardItem)
        draggingItem.setDraggingFrame(self.bounds, contents:self.bedImage)
        
        //3.
        beginDraggingSession(with: [draggingItem], event: theEvent, source: self)
    }
    
    
    
    
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        // Drawing code here.
    }
    
}
