//
//  ImagePickerItem.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 27/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

class ImagePickerItem: NSCollectionViewItem {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    @IBOutlet weak var pickerImageView: NSImageView!
}
