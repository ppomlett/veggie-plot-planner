//
//  SeedBoxItemItem.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 26/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

protocol SeedBoxItemItemDelegate: class {
    func doubleClickedOnItem(item: NSCollectionViewItem)
}



class SeedBoxItemItem: NSCollectionViewItem {
    weak var delegate: SeedBoxItemItemDelegate?
    @IBOutlet weak var itemImageView: NSImageView!
    @IBOutlet weak var nameLabel: NSTextField!
    @IBOutlet weak var descriptionLabel: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    override var isSelected: Bool {
        didSet {
            print("Selection changed")
            if isSelected == true {
            self.descriptionLabel.stringValue = "Selected"
                self.view.wantsLayer = true
               // self.view.layer?.backgroundColor = NSColor.blue.cgColor
                self.view.layer?.borderWidth = 1
                self.view.layer?.borderColor = NSColor.blue.cgColor
                self.view.layer?.cornerRadius = 5
        }
            else{
              self.descriptionLabel.stringValue = "Not Selected"
                self.view.wantsLayer = true
                self.view.layer?.borderColor = NSColor.white.cgColor
            }
        }
    }
    
    
    
    
    
    override func mouseDown(with event: NSEvent) {
        super.mouseDown(with: event)
        if(event.clickCount == 2){
            self.delegate?.doubleClickedOnItem(item: self)
          
            
        }
        
        
    }
    
    
    
}
