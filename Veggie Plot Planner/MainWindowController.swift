//
//  MainWindowController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 24/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController, NSWindowDelegate {
let dataHelper = DataHelper.sharedInstance
    
    override func windowDidLoad() {
        super.windowDidLoad()
    self.window?.delegate = self
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

    
    
    func windowWillReturnUndoManager(_ window: NSWindow) -> UndoManager? {
            // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
           
    
            return dataHelper.managedObjectContext.undoManager
        }
    
    
    
    
    
}
