//
//  AddSeedItemViewController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 26/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa


class AddSeedItemViewController: NSViewController, ImagePickerViewControllerDelegate {
    
    let dataHelper = DataHelper.sharedInstance
    @IBOutlet weak var nameTextField: NSTextField!
    @IBOutlet weak var descriptionTextField: NSTextField!
    @IBOutlet weak var seedImageView: NSImageView!
    @IBOutlet weak var editTypeLabel: NSTextField!
    @IBOutlet weak var familyPopUp: NSPopUpButton!
    @IBOutlet weak var plantOutfromMonthSection: NSPopUpButton!
    @IBOutlet weak var plantOutUntilMonthSection: NSPopUpButton!
    @IBOutlet weak var plantOutFromMonth: NSPopUpButton!
  
    @IBOutlet weak var plantOutUntilMonth: NSPopUpButton!
    @IBOutlet weak var harvestFromMonthSection: NSPopUpButton!
    @IBOutlet weak var harvestUntilMonthSection: NSPopUpButton!
    @IBOutlet weak var harvestFromMonth: NSPopUpButton!
    @IBOutlet weak var harvestUntilMonth: NSPopUpButton!
    
    
    
    
    
    
    
    var seedItemName = ""
    var seedItemDescription = ""
    var seedItemImageName = "cab"
    var plantFromSection = 0
    var plantFromMonth = 1
    var plantUntillSection = 10
    var plantUntillMonth = 1
    
    var harvestStartSection = 10
    var harvestStartMonth = 1
    var harvestEndSection = 20
    var harvestEndMonth = 1
    
    
    
    
    
    
    var editType = "New"
    var seedItemForEdit: SeedBoxItem!
    let monthArray:Array = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    let monthSectionArray = ["Early", "Mid", "Late"]
    
    
    
    
    
    
    
    @IBAction func addSeedItemAction(_ sender: NSButton) {
        if(self.editType == "New" || self.editType == "Duplicate" ){
        self.seedItemName = nameTextField.stringValue
        self.seedItemDescription = descriptionTextField.stringValue
        let seedBoxItem = dataHelper.createSeedBoxItem()
        seedBoxItem.seedName = self.seedItemName
        seedBoxItem.seedDescription = self.seedItemDescription
        seedBoxItem.imageName = self.seedItemImageName
            seedBoxItem.plantFromMonthSection = Int16(self.plantFromSection)
            seedBoxItem.plantFromMonth = Int16(self.plantFromMonth)
            seedBoxItem.plantUntillMonthSection = Int16(self.plantUntillSection)
            seedBoxItem.plantUntillMonth = Int16(self.plantUntillMonth)
            seedBoxItem.harvestFromMonthSection = Int16(self.harvestStartSection)
            seedBoxItem.harvestFromMonth = Int16(self.harvestStartMonth)
            seedBoxItem.harvestUntillMonthSection = Int16(self.harvestEndSection)
            seedBoxItem.harvestUntillMonth = Int16(self.harvestEndMonth)
        }
        else {
            self.seedItemForEdit.seedName = self.seedItemName
            self.seedItemForEdit.seedDescription = self.seedItemDescription
            self.seedItemForEdit.imageName = self.seedItemImageName
            
        }
        self.editType = "New"
        self.dismiss(self)
    }
    
    @IBAction func cancelAddVegAction(_ sender: NSButton) {
        self.dismiss(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.editTypeLabel.stringValue = self.editType
        if (self.editType == "Edit" || self.editType == "Duplicate") {
            self.seedItemName = seedItemForEdit.seedName!
          nameTextField.stringValue = self.seedItemName
            self.seedItemDescription = seedItemForEdit.seedDescription!
           descriptionTextField.stringValue = self.seedItemDescription
            self.seedItemImageName = seedItemForEdit.imageName!
           self.seedImageView.image = NSImage(named: self.seedItemImageName)
          //  self.editType = "New"
            
        }
        self.setItemsforPopUps()
        // Do view setup here.
    }
    
    
    func setItemsforPopUps(){
        self.familyPopUp.removeAllItems()
     self.familyPopUp.addItems(withTitles:[ "Other", "Cucurbits", "Amaranthaceae", "Asteraceae", "Brassicas", "ONIONS", "Legumes", "GRASSES", "parsley"] )
        
        self.plantOutfromMonthSection.removeAllItems()
        self.plantOutfromMonthSection.addItems(withTitles: self.monthSectionArray)
        self.plantOutFromMonth.removeAllItems()
        self.plantOutFromMonth.addItems(withTitles: self.monthArray)
        self.plantOutUntilMonthSection.removeAllItems()
        self.plantOutUntilMonthSection.addItems(withTitles: self.monthSectionArray)
        self.plantOutUntilMonth.removeAllItems()
        self.plantOutUntilMonth.addItems(withTitles: self.monthArray)
        self.harvestFromMonthSection.removeAllItems()
        self.harvestFromMonthSection.addItems(withTitles: self.monthSectionArray)
        self.harvestFromMonth.removeAllItems()
        self.harvestFromMonth.addItems(withTitles: self.monthArray)
        self.harvestUntilMonthSection.removeAllItems()
        self.harvestUntilMonthSection.addItems(withTitles: self.monthSectionArray)
        self.harvestUntilMonth.removeAllItems()
        self.harvestUntilMonth.addItems(withTitles: self.monthArray)
        
    }
    
    
    @IBAction func plantOutFromSectionAction(_ sender: NSPopUpButton) {
        if( sender.indexOfSelectedItem == 0){
         self.plantFromSection = 0
        }
        else if( sender.indexOfSelectedItem == 1){
         self.plantFromSection = 10
        }
        else {
         self.plantFromSection = 20
        }
    }
    
    @IBAction func plantOutUntillSectionAction(_ sender: NSPopUpButton) {
        if( sender.indexOfSelectedItem == 0){
            self.plantUntillSection = 10
        }
        else if( sender.indexOfSelectedItem == 1){
            self.plantUntillSection = 20
        }
        else {
            self.plantUntillSection = 30
        }
    }
    
    @IBAction func plantOutFromMonthAction(_ sender: NSPopUpButton) {
            self.plantFromMonth = monthForPopUpSelection(index: sender.indexOfSelectedItem)
    }
    
    @IBAction func plantUntillMonthAction(_ sender: NSPopUpButton) {
        self.plantUntillMonth = monthForPopUpSelection(index: sender.indexOfSelectedItem)
    }

    func monthForPopUpSelection(index: Int) -> Int {
   return index + 1
    }
    
    
    @IBAction func harvestFromSectionAction(_ sender: NSPopUpButton) {
        if( sender.indexOfSelectedItem == 0){
            self.harvestStartSection = 0
        }
        else if( sender.indexOfSelectedItem == 1){
            self.harvestStartSection = 10
        }
        else {
            self.harvestStartSection = 20
        }
        
    }
    
    @IBAction func harvestUntillSectionAction(_ sender: NSPopUpButton) {
        if( sender.indexOfSelectedItem == 0){
            self.harvestEndSection = 10
        }
        else if( sender.indexOfSelectedItem == 1){
            self.harvestEndSection = 20
        }
        else {
            self.harvestEndSection = 30
        }
        
    }
    
   
    
    @IBAction func harvestFromMonthAction(_ sender: NSPopUpButtonCell) {
        self.harvestStartMonth = monthForPopUpSelection(index: sender.indexOfSelectedItem)
        
    }
    
    @IBAction func harvestUntillMonthAction(_ sender: NSPopUpButton) {
         self.harvestEndMonth = monthForPopUpSelection(index: sender.indexOfSelectedItem)
        
    }
    
    
    
    
    //imagePickerDelegate
     func imageNamed(name: String){
        self.seedItemImageName = name
        self.seedImageView.image = NSImage(named: name)
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "picker"{
            let imagePickerViewController = segue.destinationController as! ImagePickerViewController
            
            imagePickerViewController.delegate = self
            
        }
    }
}
