//
//  PlantView.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 31/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

class PlantView: DraggableView {

    let notificationName = Notification.Name("MouseDownOnPlantNotification")
    
    override func mouseDown(with event: NSEvent) {
        super.mouseDown(with: event)
        //  self.isDragged = false
        // self.selected = true
        NotificationCenter.default.post(name: notificationName, object: self)
        
    }
    let dragNotificationName = Notification.Name("MouseDraggedOnPlantNotification")
    
    override func mouseDragged(with event: NSEvent) {
        super.mouseDragged(with: event)
        NotificationCenter.default.post(name: dragNotificationName, object: self)
    }
    let mouseUpNotificationName = Notification.Name("MouseUpOnPlantNotification")
    override func mouseUp(with event: NSEvent) {
        super.mouseUp(with: event)
        NotificationCenter.default.post(name: mouseUpNotificationName, object: self.frame)
    }
  
    
    func resize(_ image: NSImage, w: Int, h: Int) -> NSImage {
        let startSize = NSMakeSize(CGFloat(40), CGFloat(40))
        let endImage = NSImage(size: startSize)
        endImage.lockFocus()
        image.draw(in: NSMakeRect(0, 0, startSize.width, startSize.height), from: NSMakeRect(0, 0, image.size.width, image.size.height), operation: NSCompositingOperation.sourceOver, fraction: CGFloat(1))
        endImage.unlockFocus()
        endImage.size = startSize
        let image2 = NSImage(data: endImage.tiffRepresentation!)!
        
        
        
        
        
        let destSize = NSMakeSize(CGFloat(w), CGFloat(h))
        let newImage = NSImage(size: destSize)
        newImage.lockFocus()
        image2.draw(in: NSMakeRect(0, 0, image2.size.width, image2.size.height), from: NSMakeRect(0, 0, image2.size.width, image2.size.height), operation: NSCompositingOperation.sourceOver, fraction: CGFloat(1))
        newImage.unlockFocus()
        newImage.size = destSize
        return NSImage(data: newImage.tiffRepresentation!)!
    }
    
    
    
    
    func jresize(_ image: NSImage, w: Int, h: Int) -> NSImage {
        let destSize = NSMakeSize(CGFloat(w), CGFloat(h))
        let newImage = NSImage(size: destSize)
        newImage.lockFocus()
        image.draw(in: NSMakeRect(0, 0, destSize.width, destSize.height), from: NSMakeRect(0, 0, image.size.width, image.size.height), operation: NSCompositingOperation.sourceOver, fraction: CGFloat(1))
        newImage.unlockFocus()
        newImage.size = destSize
        return NSImage(data: newImage.tiffRepresentation!)!
    }
    
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
     
        // Drawing code here.
        
        
    }
    
    
    func backGroundImage(image: NSImage, width: CGFloat , hieght: CGFloat){
        
        // let resizedImage = self.resize(NSImage(named: "cab")!, w: 40, h: 40)
        let resizedImage = self.resize(image, w: Int(width), h: Int(hieght))
        self.layer?.backgroundColor = NSColor(patternImage:resizedImage).cgColor
        self.layer?.borderWidth = 1
        self.layer?.borderColor = #colorLiteral(red: 0.5147954737, green: 0.3388757147, blue: 0.06842950669, alpha: 1).cgColor
        
        
    }
    
    
    
    
    
    
    override init(frame frameRect: NSRect) {
        super.init(frame:frameRect);
       
         
        
    }
    
    required init(coder: NSCoder) {
        
        
        
       
        super.init(coder: coder)!
        
        
        
    }
    
    //or customized constructor/ init
    init(frame frameRect: NSRect, otherInfo:Int) {
        super.init(frame:frameRect);
        // other code
    }
    
}
