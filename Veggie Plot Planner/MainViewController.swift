//
//  MainViewController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 26/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

class MainViewController: NSViewController, NSFetchedResultsControllerDelegate, NSTableViewDelegate, NSOutlineViewDelegate ,SeedBoxItemInfoDelegate, PlotViewDelegate, BedControllerDelegate, TimeLineViewControllerDelegate  {
    
    //Use only dataHelper helper methods to access core data stack
    let dataHelper = DataHelper.sharedInstance
    
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet var plotArrayController: NSArrayController!
    @IBOutlet var treeController: NSTreeController!
    
    
    
    @IBOutlet weak var rightSplitView: NSSplitView!
    
    @IBOutlet weak var splitView: NSView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  self.splitView.isHidden = true
        
        self.setUpSeedBoxCollectionView()
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let seedBoxItemInfoVC = storyboard.instantiateController(withIdentifier: "info")
        self.seedBoxItemInfoVC = seedBoxItemInfoVC as! SeedBoxItemInfoViewController
        self.seedBoxItemInfoVC.delegate = self
        self.outlineView.delegate = self
        self.plotView.wantsLayer = true
        plotView.layer?.backgroundColor =  #colorLiteral(red: 0.967862308, green: 1, blue: 0.9348346591, alpha: 1) .cgColor                //NSColor.green.cgColor
        self.plotView.delegate = self
        self.scrollView.documentView = self.plotView
        autoLayout(contentView: self.plotView)
        self.tableView.delegate = self
        self.bedController.delegate = self
        self.bedController.loadDraggableViewsAndObjects(parentObject: self.plotArrayController.selectedObjects.first as! NSManagedObject?, parentView: self.plotView)
        
        self.scrollView.hasVerticalRuler = true
        self.scrollView.hasHorizontalRuler = true
       // self.scrollView.horizontalRulerView?.addMarker(rulerMarker)
        self.scrollView.rulersVisible = true
    }
    ////creating a controller
    let bedController = BedController()
    
    @IBOutlet weak var outlineView: NSOutlineView!
    
    
    func outlineViewSelectionDidChange(_: Notification){
        print("selection changed")
    }
    
    // for testing treeController
    @IBAction func testAction(_ sender: Any) {
        let a = self.treeController.selectedObjects.first
        if let b = a as? Plot {
            print(b.name!)
        }
    }
    
    lazy var seedBoxItemInfoVC: SeedBoxItemInfoViewController = SeedBoxItemInfoViewController()
    var selectedSeedBoxItem: SeedBoxItem!
    
    // MARK: -  setup seedBoxItem collectionView.
    //Check MainViewController extention for dataSource and Delegate methods.
    @IBOutlet weak var collectionView: NSCollectionView!
    
    lazy var seedBoxItems: NSFetchedResultsController<SeedBoxItem> = {
        let fetchRequest: NSFetchRequest<SeedBoxItem> = SeedBoxItem.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "seedName", ascending: true)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.dataHelper.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    func setUpSeedBoxCollectionView(){
        do {
            try self.seedBoxItems.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        let nib = NSNib(nibNamed: "SeedBoxItemItem", bundle: nil)
        self.collectionView.register(nib, forItemWithIdentifier: "item")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    // MARK: - SeedBoxItemInfoDelegate methods
    var editType = "New"
    var seedBoxItemForEdit: SeedBoxItem!
    
    func duplicateSeedBoxItem(item: SeedBoxItem){
        self.editType = "Duplicate"
        self.seedBoxItemForEdit = item
        self.performSegue(withIdentifier: "addNewSeed", sender: self)
    }
    
    func editSeedBoxItem(item: SeedBoxItem){
        self.editType = "Edit"
        self.seedBoxItemForEdit = item
        self.performSegue(withIdentifier: "addNewSeed", sender: self)
    }
    
    
    
    var timeLineViewController: TimeLineViewController!
    // TimeLineDelegateMethod
    func dateSelectionChanged(date: NSDate){
       self.bedController.selectedDate = date
        self.bedController.reLoadDraggableViewsAndObjects(parentObject: self.plotArrayController.selectedObjects.first as! NSManagedObject?, parentView: plotView)
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "addNewSeed"{
            let addSeedItemViewController = segue.destinationController as! AddSeedItemViewController
            addSeedItemViewController.editType = self.editType
            addSeedItemViewController.seedItemForEdit = self.seedBoxItemForEdit
        }
        if segue.identifier == "timeLine"{
            let timeLineViewController = segue.destinationController as! TimeLineViewController
            self.timeLineViewController = timeLineViewController
            self.timeLineViewController.delegate = self
          //  addSeedItemViewController.editType = self.editType
          //  addSeedItemViewController.seedItemForEdit = self.seedBoxItemForEdit
        }
        
        
    }
    
    // MARK: CODE FOR SETTING CONSTRAINTS ON DYNAMIC SCROLLVIEW
    
    @IBOutlet weak var scrollView: NSScrollView!
    
    @IBOutlet weak var plotView: PlotView!
    
    var docHeightConstraint: NSLayoutConstraint?
    var docWidthConstraint: NSLayoutConstraint?
    
    func autoLayout(contentView: NSView){
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        let topPinContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.top
            , relatedBy: .equal
            , toItem: contentView.superview
            , attribute: NSLayoutAttribute.top
            , multiplier: 1.0
            , constant: 0
        )
        contentView.superview?.addConstraint(topPinContraints)
        
        let leadPinContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.leading
            , relatedBy: .equal
            , toItem: contentView.superview
            , attribute: NSLayoutAttribute.leading
            , multiplier: 1.0
            , constant: 0
        )
        contentView.superview?.addConstraint(leadPinContraints)
        
        //        let bottomPinContraints = NSLayoutConstraint(
        //            item: contentView
        //            , attribute: NSLayoutAttribute.right
        //            , relatedBy: .greaterThanOrEqual
        //            , toItem: contentView.superview
        //            , attribute: NSLayoutAttribute.right
        //            , multiplier: 1.0
        //            , constant: 0
        //        )
        // contentView.superview?.addConstraint(bottomPinContraints)
        let heightContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.height
            , relatedBy: .equal
            , toItem:  nil                    //     contentView.superview!
            , attribute: NSLayoutAttribute.height
            , multiplier: 1.0
            , constant: 2000
        )
        contentView.superview?.addConstraint(heightContraints)
        //  let calculatedWith: CGFloat = (CGFloat) (contentView.subviews.count * 130)
        docWidthConstraint = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.width
            , relatedBy: .equal
            , toItem: nil
            , attribute: NSLayoutAttribute.width
            , multiplier: 1.0
            , constant: 0
        )
        docWidthConstraint?.constant =  2000           //calculatedWith
        contentView.superview?.addConstraint(docWidthConstraint!)
    }
    
    //Deselect view if mouse down is on plotView
    func mouseDownOnCanvas(_ sender: PlotView){
        if (self.bedController.selectedView != nil){
            self.bedController.selectedView?.selected = false
            self.bedController.selectedView?.setNeedsDisplay((self.bedController.selectedView?.bounds)!)
        }
    }
    
    //Creation of a new bed object
    func processImage(_ image: NSImage, center: NSPoint, onView:PlotView ){
        let plot = plotArrayController.selectedObjects.first as! Plot
        self.bedController.createDraggableViewAndObject(atPiont: center, onView: onView, parentObject: plot)
        
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        self.bedController.reLoadDraggableViewsAndObjects(parentObject: self.plotArrayController.selectedObjects.first as! NSManagedObject?, parentView: plotView)
    }
    //BedController delegate methods
    
    @IBOutlet weak var tempNameLable: NSTextField!
    @IBOutlet weak var tempDescriptionLabel: NSTextField!
    
    func bedSelected(bed: Bed){
     tempNameLable.stringValue = bed.name!
        print("BED SELECT")
        
    }
    func plantSelected(plant: Plant){
      tempNameLable.stringValue = plant.name!
       print("PLANT SELECT")
        
    }
    func plotSelected() {
   let plot =  plotArrayController.selectedObjects.first as! Plot
      tempNameLable.stringValue = plot.name!
         print("PLOT SELECT")
    }
    
    
    
    
    
    
    //Temp action for creating new plots
    @IBAction func addObject(_ sender: AnyObject) {
        let plot = dataHelper.createPlot()
        plot.name = "New Plot"
    }
    
    //Temp call to save managed objects
    @IBAction func save(_ sender: AnyObject) {
        dataHelper.saveChanges()
    }
    
}
