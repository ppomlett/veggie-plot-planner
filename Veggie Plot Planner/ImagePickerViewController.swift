//
//  ImagePickerViewController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 26/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa


protocol ImagePickerViewControllerDelegate: class {
    func imageNamed(name: String)
}




class ImagePickerViewController: NSViewController, NSCollectionViewDelegate, NSCollectionViewDataSource {

    weak var delegate: ImagePickerViewControllerDelegate?
    @IBOutlet weak var collectionView: NSCollectionView!
    
    
    
    var AC:NSMutableArray = NSMutableArray()
    var imageArray = Array<String>()
    
    // MARK: Collection View Data Source / Delegate
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        
        
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
       
        
        return self.imageArray.count
        
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt
        indexPath: IndexPath) -> NSCollectionViewItem {
        // Recycle or create an item.
        let item = self.collectionView.makeItem(withIdentifier: "item", for: indexPath) as! ImagePickerItem
        
        // Configure the item with an image from the app's data structures
      //  let image = (self.AC.object(at: indexPath.item) as! ImagePickerObject).image
        // let a =  ImagePickerObject
        
        
        let name = self.imageArray[indexPath.item]
        let image = NSImage(named: name)
        
        
        
        item.pickerImageView.image = image
        
        return item
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        
        
       // let image = (self.AC.object(at: (indexPaths.first?.item)!) as! ImagePickerObject).image
        
         let name = self.imageArray[(indexPaths.first?.item)!]
        self.delegate?.imageNamed(name: name)
     
        self.dismiss(self)
        
    }
    
    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = NSNib(nibNamed: "ImagePickerItem", bundle: nil)
        
        collectionView.register(nib, forItemWithIdentifier: "item")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        print("image picker called")
        
        //   self.collectionView.itemPrototype = self.storyboard?.instantiateController(withIdentifier: "imageItem") as? NSCollectionViewItem
        
        
        let a = "parsnip"
        let b = "sprouts"
        let c = "carrot"
        let d = "red"
        let e = "spuds"
        let f = "onion"
        let g = "cab"
          let h = "broccoli"
        self.imageArray = [a, b, c, d, e, f, g,h ]
        
        
      
        self.collectionView.reloadData()
    }
    
}
