//
//  BedView.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 31/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

protocol BedViewDelegate {
    //  func processImageURLs(_ urls: [URL], center: NSPoint)
    func processImage(_ image: NSImage, center: NSPoint, onView:BedView )
    // func processAction(_ action: String, center: NSPoint)
}





class BedView: DraggableView {
  //  kUTTypeTIFF

    
    let notificationName = Notification.Name("MouseDownOnBedNotification")
    
    override func mouseDown(with event: NSEvent) {
        super.mouseDown(with: event)
        //  self.isDragged = false
        // self.selected = true
        NotificationCenter.default.post(name: notificationName, object: self)
        
    }
    let dragNotificationName = Notification.Name("MouseDraggedNotification")
    
    override func mouseDragged(with event: NSEvent) {
        super.mouseDragged(with: event)
        NotificationCenter.default.post(name: dragNotificationName, object: self)
    }
        let mouseUpNotificationName = Notification.Name("MouseUpOnBedNotification")
        override func mouseUp(with event: NSEvent) {
            super.mouseUp(with: event)
            NotificationCenter.default.post(name: mouseUpNotificationName, object: self.frame)
        }
    
   var delegate: BedViewDelegate?
    
    var nonURLTypes: Set<String>  { return [String(kUTTypeUTF8PlainText)] }
    var acceptableTypes: Set<String> { return nonURLTypes.union([NSURLPboardType]) }
  
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        self.layer?.backgroundColor = NSColor(patternImage:#imageLiteral(resourceName: "soilTile") ).cgColor
        
                #imageLiteral(resourceName: "raisedFrame") .draw(in: self.bounds, from: NSZeroRect, operation: NSCompositingOperation.sourceOver, fraction: 1)
        self.shadow = NSShadow()
        self.layer?.shadowOpacity = 0.5
        self.layer?.shadowColor = NSColor.black.cgColor
        self.layer?.shadowOffset = NSMakeSize(2, -2)
        self.layer?.shadowRadius = 2
       // self.layer?.masksToBounds = true
    }
    
    
    func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
        return true
    }
    
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        Swift.print("entered")
        return  NSDragOperation.copy            //allow ? .copy : NSDragOperation()
    }
    
    
    override func draggingExited(_ sender: NSDraggingInfo?) {
        
    }
    
    override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
      // let allow = shouldAllowDrag(sender)
        return true
    }
    
     let seedBoxItemDroppedNotification = Notification.Name("seedBoxItemDropped")
    
    var dropPoint: NSPoint?
    
    override func performDragOperation(_ draggingInfo: NSDraggingInfo) -> Bool {
   Swift.print("Drop on Bed")
        let point = convert(draggingInfo.draggingLocation(), from: nil)
      
      self.dropPoint = point
        
        
 NotificationCenter.default.post(name: seedBoxItemDroppedNotification, object: self)
       return true
        
    }
    

    
    
    override init(frame frameRect: NSRect) {
        super.init(frame:frameRect);
         self.register(forDraggedTypes: Array(acceptableTypes))
        
       // self.register(forDraggedTypes: [(kUTTypeUTF8PlainText) as String])   
        //  self.layer?.backgroundColor =  NSColor.brown.cgColor
        self.selected = true
        Swift.print("THE BEDVIEW IS INIT")
    }
    // var acceptableTypes: Set<String> { return [NSURLPboardType] }
    
    
    
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //   self.wantsLayer = true
        //  self.layer?.backgroundColor = NSColor.brown.cgColor
        self.register(forDraggedTypes: Array(acceptableTypes))
        Swift.print("called")
    }
    
}
