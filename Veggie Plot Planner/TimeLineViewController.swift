//
//  TimeLineViewController.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 07/11/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

protocol TimeLineViewControllerDelegate: class {
    func dateSelectionChanged(date: NSDate)
}


class TimeLineViewController: NSViewController {
    
    weak var delegate: TimeLineViewControllerDelegate?
    @IBOutlet var documentView: NSView!
    
    func autoLayout(contentView: NSView){
        contentView.translatesAutoresizingMaskIntoConstraints = false
        let topPinContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.top
            , relatedBy: .equal
            , toItem: contentView.superview
            , attribute: NSLayoutAttribute.top
            , multiplier: 1.0
            , constant: 0
        )
        contentView.superview?.addConstraint(topPinContraints)
        let leadPinContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.centerX
            , relatedBy: .equal
            , toItem: contentView.superview
            , attribute: NSLayoutAttribute.centerX
            , multiplier: 1.0
            , constant: 0
        )
        contentView.superview?.addConstraint(leadPinContraints)
        let heightContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.height
            , relatedBy: .equal
            , toItem:  nil
            , attribute: NSLayoutAttribute.height
            , multiplier: 1.0
            , constant: 176
        )
        contentView.superview?.addConstraint(heightContraints)
        let widthContraints = NSLayoutConstraint(
            item: contentView
            , attribute: NSLayoutAttribute.width
            , relatedBy: .equal
            , toItem: nil
            , attribute: NSLayoutAttribute.width
            , multiplier: 1.0
            , constant: 875
        )
        contentView.superview?.addConstraint(widthContraints)
    }
    
    @IBOutlet weak var scrollView: NSScrollView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundview: NSView!
    @IBOutlet weak var view1: NSView!
    @IBOutlet weak var view1Const: NSLayoutConstraint!
    @IBOutlet weak var imageView1: NSImageView!
    @IBOutlet weak var label1: NSTextField!
    @IBOutlet weak var view2: NSView!
    @IBOutlet weak var view2Const: NSLayoutConstraint!
    @IBOutlet weak var imageView2: NSImageView!
    @IBOutlet weak var label2: NSTextField!
    @IBOutlet weak var view3: NSView!
    @IBOutlet weak var view3Const: NSLayoutConstraint!
    @IBOutlet weak var imageView3: NSImageView!
    @IBOutlet weak var label3: NSTextField!
    @IBOutlet weak var view4: NSView!
    @IBOutlet weak var view4Const: NSLayoutConstraint!
    @IBOutlet weak var imageView4: NSImageView!
    @IBOutlet weak var label4: NSTextField!
    @IBOutlet weak var view5: NSView!
    @IBOutlet weak var view5Const: NSLayoutConstraint!
    @IBOutlet weak var imageView5: NSImageView!
    @IBOutlet weak var label5: NSTextField!
    @IBOutlet weak var view6: NSView!
    @IBOutlet weak var view6Const: NSLayoutConstraint!
    @IBOutlet weak var imageView6: NSImageView!
    @IBOutlet weak var label6: NSTextField!
    @IBOutlet weak var view7: NSView!
    @IBOutlet weak var view7Const: NSLayoutConstraint!
    @IBOutlet weak var imageView7: NSImageView!
    @IBOutlet weak var label7: NSTextField!
    @IBOutlet weak var view8: NSView!
    @IBOutlet weak var view8Const: NSLayoutConstraint!
    @IBOutlet weak var imageView8: NSImageView!
    @IBOutlet weak var label8: NSTextField!
    @IBOutlet weak var view9: NSView!
    @IBOutlet weak var view9Const: NSLayoutConstraint!
    @IBOutlet weak var imageView9: NSImageView!
    @IBOutlet weak var label9: NSTextField!
    @IBOutlet weak var view10: NSView!
    @IBOutlet weak var view10Const: NSLayoutConstraint!
    @IBOutlet weak var imageView10: NSImageView!
    @IBOutlet weak var label10: NSTextField!
    @IBOutlet weak var view11: NSView!
    @IBOutlet weak var view11Const: NSLayoutConstraint!
    @IBOutlet weak var imageview11: NSImageView!
    @IBOutlet weak var label11: NSTextField!
    @IBOutlet weak var view12: NSView!
    @IBOutlet weak var view12Const: NSLayoutConstraint!
    @IBOutlet weak var imageView12: NSImageView!
    @IBOutlet weak var label12: NSTextField!
    @IBOutlet weak var view13: NSView!
    @IBOutlet weak var view13Const: NSLayoutConstraint!
    @IBOutlet weak var imageView13: NSImageView!
    @IBOutlet weak var label13: NSTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate?.dateSelectionChanged(date: NSDate())
        self.scrollView.documentView = self.documentView
        self.autoLayout(contentView: self.documentView)
        self.backgroundview.wantsLayer = true
        self.backgroundview.layer?.borderWidth = 1
        self.backgroundview.layer?.borderColor = NSColor.gray.cgColor
        let today = NSDate()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: today as Date)
        let month = calendar.component(.month, from: today as Date)
        let day = calendar.component(.day, from: today as Date)
        self.nextYearLabel.intValue = Int32(year)
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.full
        self.formatter = formatter
        self.thisYear = year
        self.thisMonth =   month
        self.thisDay = day
        self.updateTheLabels()
        self.leadingConstraint.constant = CGFloat(thisDay * -2)
        self.setConstraintsForViews()
        
    }
    
    func setConstraintsForViews(){
        let daysInMonth = self.daysInMonthFormthisMonth()
        self.view1Const.constant = CGFloat(daysInMonth[11]) * 2
        print("view1")
        print( CGFloat(daysInMonth[11]) * 2)
        self.view2Const.constant = CGFloat(daysInMonth[10]) * 2
        print("view2")
        print( CGFloat(daysInMonth[10]) * 2)
        self.view3Const.constant = CGFloat(daysInMonth[9]) * 2
        print("view3")
        print( CGFloat(daysInMonth[9]) * 2)
        self.view4Const.constant = CGFloat(daysInMonth[8]) * 2
        print("view4")
        print( CGFloat(daysInMonth[8]) * 2)
        self.view5Const.constant = CGFloat(daysInMonth[7]) * 2
        print("view5")
        print( CGFloat(daysInMonth[7]) * 2)
        self.view6Const.constant = CGFloat(daysInMonth[6]) * 2
        print("view6")
        print( CGFloat(daysInMonth[6]) * 2)
        self.view7Const.constant = CGFloat(daysInMonth[5]) * 2
        print("view7")
        print( CGFloat(daysInMonth[5]) * 2)
        self.view8Const.constant = CGFloat(daysInMonth[4]) * 2
        print("view8")
        print( CGFloat(daysInMonth[4]) * 2)
        self.view9Const.constant = CGFloat(daysInMonth[3]) * 2
        print("view9")
        print( CGFloat(daysInMonth[3]) * 2)
        self.view10Const.constant = CGFloat(daysInMonth[2]) * 2
        print("view10")
        print( CGFloat(daysInMonth[2]) * 2)
        self.view11Const.constant = CGFloat(daysInMonth[1]) * 2
        print("view11")
        print( CGFloat(daysInMonth[1]) * 2)
        self.view12Const.constant = CGFloat(daysInMonth[0]) * 2
        print("view12")
        print( CGFloat(daysInMonth[0]) * 2)
        self.view13Const.constant = CGFloat(daysInMonth[12]) * 2
    }
    
    func numberOfDaysBetweenNowAndMonth(month:Int)-> Int{
        let calendar = Calendar.current
        let now = NSDate()
        let date = calendar.date(from: DateComponents(year: self.thisYear, month: month , day: 1))!
        let components = calendar.dateComponents([Calendar.Component.day], from: now as Date, to: date)
        return components.day!
    }
    
    func daysInMonthFormthisMonth() -> [Int] {
        let calendar = Calendar.current
        var monthArray = [Int]()
        var i = 0
        while i <= 12 {
            let date = calendar.date(from: DateComponents(year: self.thisYear, month: self.thisMonth + i , day: self.thisDay))!
            let days = calendar.range(of: .day, in: .month, for: date)
            print(days!.count)
            monthArray.append((days?.count)!)
            i = i + 1
        }
        return monthArray
    }
    
    var thisYear:Int = 0
    var thisDay:Int = 0
    var thisMonth:Int = 0
    var seedItemPlantingImageView: NSImageView!
    var selectedSeedItemViewController: SelectedSeedItemViewcontroller!
    
    func updateUIForSelectedSeedItem(seedItem: SeedBoxItem){
        self.selectedseedBoxItem(seedBoxItem: seedItem)
        //create planting image view for seedBoxItem
        if (self.seedItemPlantingImageView != nil) {
            self.seedItemPlantingImageView.removeFromSuperview()
            self.seedItemPlantingImageView = nil
        }
        let plantDate = self.plantOutDate
        let harvestDate = self.harvestDate
        let daysBetweenNowAndPlantOut = Calendar.current.dateComponents([Calendar.Component.day], from: NSDate() as Date, to: plantDate as! Date).day!
        let daysBetweenPlantOutAndHarvest = Calendar.current.dateComponents([Calendar.Component.day], from: plantDate as! Date as Date, to: harvestDate as! Date).day!
        let hViewX = daysBetweenNowAndPlantOut * 2
        let hViewWidth = daysBetweenPlantOutAndHarvest  * 2
        let harvestImage = #imageLiteral(resourceName: "harvest")
        self.seedItemPlantingImageView = NSImageView(frame: NSRect(x: hViewX, y: 40, width: hViewWidth, height: 20))
        self.seedItemPlantingImageView.imageScaling = NSImageScaling(rawValue: 1)!
        self.seedItemPlantingImageView.image = harvestImage
        self.backgroundview.addSubview(self.seedItemPlantingImageView)
        //Create pop over view controller for selected seed item
        if self.selectedSeedItemViewController == nil {
            let storyboard = NSStoryboard(name: "Main", bundle: nil)
            let selectedSeedBoxItemviewController = storyboard.instantiateController(withIdentifier: "selected")
            self.selectedSeedItemViewController = selectedSeedBoxItemviewController as! SelectedSeedItemViewcontroller
            self.selectedSeedItemViewController.seedBoxItem = self.seedBoxItem
            self.presentViewController(self.selectedSeedItemViewController, asPopoverRelativeTo: (self.backgroundview.bounds), of: (self.backgroundview), preferredEdge: NSRectEdge.maxX , behavior: NSPopoverBehavior.applicationDefined)
        }
        else if self.selectedSeedItemViewController.isViewLoaded == true && self.selectedSeedItemViewController.view.window == nil  {
            self.presentViewController(self.selectedSeedItemViewController, asPopoverRelativeTo: (self.backgroundview.bounds), of: (self.backgroundview), preferredEdge: NSRectEdge.maxX , behavior: NSPopoverBehavior.applicationDefined)
            self.selectedSeedItemViewController.seedBoxItem = self.seedBoxItem
            self.selectedSeedItemViewController.updateUI()
        }
        else{
            self.selectedSeedItemViewController.seedBoxItem = self.seedBoxItem
            self.selectedSeedItemViewController.updateUI()
        }
    }
    
    func updateTheLabels(){
        switch thisMonth {
        case  1:
            print(1)
            label1.stringValue = "Dec" ; label2.stringValue = "Nov"; label3.stringValue = "Oct"; label4.stringValue = "Sept"; label5.stringValue = "Aug"; label6.stringValue = "July"; label7.stringValue = "June"; label8.stringValue = "May"; label9.stringValue = "April"; label10.stringValue = "March"; label11.stringValue = "Feb"; label12.stringValue = "Jan"; label13.stringValue = "Jan"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileB"); self.imageView2.image = #imageLiteral(resourceName: "tileBBR"); self.imageView3.image = #imageLiteral(resourceName: "tileBR"); self.imageView4.image = #imageLiteral(resourceName: "tileRB"); self.imageView5.image = #imageLiteral(resourceName: "tileRRB"); self.imageView6.image = #imageLiteral(resourceName: "tileR"); imageView7.image = #imageLiteral(resourceName: "tileR"); self.imageView8.image = #imageLiteral(resourceName: "tileRRB"); self.imageView9.image = #imageLiteral(resourceName: "tileRB"); self.imageView10.image = #imageLiteral(resourceName: "tileBR"); self.imageview11.image = #imageLiteral(resourceName: "tileBBR");  self.imageView12.image = #imageLiteral(resourceName: "tileB");  self.imageView13.image = #imageLiteral(resourceName: "tileB")
            
        case  2:
            print(2)
            label1.stringValue = "Jan" ; label2.stringValue = "Dec"; label3.stringValue = "Nov"; label4.stringValue = "Oct"; label5.stringValue = "Sept"; label6.stringValue = "Aug"; label7.stringValue = "July"; label8.stringValue = "June"; label9.stringValue = "May"; label10.stringValue = "April"; label11.stringValue = "March"; label12.stringValue = "Feb"; label13.stringValue = "Feb"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileB"); self.imageView2.image = #imageLiteral(resourceName: "tileB"); self.imageView3.image = #imageLiteral(resourceName: "tileBBR"); self.imageView4.image = #imageLiteral(resourceName: "tileBR"); self.imageView5.image = #imageLiteral(resourceName: "tileRB"); self.imageView6.image = #imageLiteral(resourceName: "tileRRB"); imageView7.image = #imageLiteral(resourceName: "tileR"); self.imageView8.image = #imageLiteral(resourceName: "tileR"); self.imageView9.image = #imageLiteral(resourceName: "tileRRB"); self.imageView10.image = #imageLiteral(resourceName: "tileRB"); self.imageview11.image = #imageLiteral(resourceName: "tileBR");  self.imageView12.image = #imageLiteral(resourceName: "tileBBR");  self.imageView13.image = #imageLiteral(resourceName: "tileBBR")
            
        case  3:
            print(3)
            label1.stringValue = "Feb" ; label2.stringValue = "Jan"; label3.stringValue = "Dec"; label4.stringValue = "Nov"; label5.stringValue = "Oct"; label6.stringValue = "Sept"; label7.stringValue = "Aug"; label8.stringValue = "July"; label9.stringValue = "June"; label10.stringValue = "May"; label11.stringValue = "April"; label12.stringValue = "March"; label13.stringValue = "March"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileBBR"); self.imageView2.image = #imageLiteral(resourceName: "tileB"); self.imageView3.image = #imageLiteral(resourceName: "tileB"); self.imageView4.image = #imageLiteral(resourceName: "tileBBR"); self.imageView5.image = #imageLiteral(resourceName: "tileBR"); self.imageView6.image = #imageLiteral(resourceName: "tileRB"); imageView7.image = #imageLiteral(resourceName: "tileRRB"); self.imageView8.image = #imageLiteral(resourceName: "tileR"); self.imageView9.image = #imageLiteral(resourceName: "tileR"); self.imageView10.image = #imageLiteral(resourceName: "tileRRB"); self.imageview11.image = #imageLiteral(resourceName: "tileRB");  self.imageView12.image = #imageLiteral(resourceName: "tileBR");  self.imageView13.image = #imageLiteral(resourceName: "tileBR")
            
        case  4:
            print(4)
            label1.stringValue = "March" ; label2.stringValue = "Feb"; label3.stringValue = "Jan"; label4.stringValue = "Dec"; label5.stringValue = "Nov"; label6.stringValue = "Oct"; label7.stringValue = "Sept"; label8.stringValue = "Aug"; label9.stringValue = "July"; label10.stringValue = "June"; label11.stringValue = "May"; label12.stringValue = "April"; label13.stringValue = "April"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileBR"); self.imageView2.image = #imageLiteral(resourceName: "tileBBR"); self.imageView3.image = #imageLiteral(resourceName: "tileB"); self.imageView4.image = #imageLiteral(resourceName: "tileB"); self.imageView5.image = #imageLiteral(resourceName: "tileBBR"); self.imageView6.image = #imageLiteral(resourceName: "tileBR"); imageView7.image = #imageLiteral(resourceName: "tileRB"); self.imageView8.image = #imageLiteral(resourceName: "tileRRB"); self.imageView9.image = #imageLiteral(resourceName: "tileR"); self.imageView10.image = #imageLiteral(resourceName: "tileR"); self.imageview11.image = #imageLiteral(resourceName: "tileRRB");  self.imageView12.image = #imageLiteral(resourceName: "tileRB");  self.imageView13.image = #imageLiteral(resourceName: "tileRB")
            
        case  5:
            print(5)
            label1.stringValue = "April" ; label2.stringValue = "March"; label3.stringValue = "Fed"; label4.stringValue = "Jan"; label5.stringValue = "Dec"; label6.stringValue = "Nov"; label7.stringValue = "Oct"; label8.stringValue = "Sept"; label9.stringValue = "Aug"; label10.stringValue = "July"; label11.stringValue = "June"; label12.stringValue = "May"; label13.stringValue = "May"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileRB"); self.imageView2.image = #imageLiteral(resourceName: "tileBR"); self.imageView3.image = #imageLiteral(resourceName: "tileBBR"); self.imageView4.image = #imageLiteral(resourceName: "tileB"); self.imageView5.image = #imageLiteral(resourceName: "tileB"); self.imageView6.image = #imageLiteral(resourceName: "tileBBR"); imageView7.image = #imageLiteral(resourceName: "tileBR"); self.imageView8.image = #imageLiteral(resourceName: "tileRB"); self.imageView9.image = #imageLiteral(resourceName: "tileRRB"); self.imageView10.image = #imageLiteral(resourceName: "tileR"); self.imageview11.image = #imageLiteral(resourceName: "tileR");  self.imageView12.image = #imageLiteral(resourceName: "tileRRB");  self.imageView13.image = #imageLiteral(resourceName: "tileRRB")
            
        case  6:
            print(6)
            label1.stringValue = "May" ; label2.stringValue = "April"; label3.stringValue = "March"; label4.stringValue = "Feb"; label5.stringValue = "Jan"; label6.stringValue = "Dec"; label7.stringValue = "Nov"; label8.stringValue = "Oct"; label9.stringValue = "Sept"; label10.stringValue = "Aug"; label11.stringValue = "July"; label12.stringValue = "June"; label13.stringValue = "June"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileRRB"); self.imageView2.image = #imageLiteral(resourceName: "tileRB"); self.imageView3.image = #imageLiteral(resourceName: "tileBR"); self.imageView4.image = #imageLiteral(resourceName: "tileBBR"); self.imageView5.image = #imageLiteral(resourceName: "tileB"); self.imageView6.image = #imageLiteral(resourceName: "tileB"); imageView7.image = #imageLiteral(resourceName: "tileBBR"); self.imageView8.image = #imageLiteral(resourceName: "tileBR"); self.imageView9.image = #imageLiteral(resourceName: "tileRB"); self.imageView10.image = #imageLiteral(resourceName: "tileRRB"); self.imageview11.image = #imageLiteral(resourceName: "tileR");  self.imageView12.image = #imageLiteral(resourceName: "tileR");  self.imageView13.image = #imageLiteral(resourceName: "tileR")
            
        case  7:
            print(7)
            label1.stringValue = "June" ; label2.stringValue = "May"; label3.stringValue = "April"; label4.stringValue = "March"; label5.stringValue = "Feb"; label6.stringValue = "Jan"; label7.stringValue = "Dec"; label8.stringValue = "Nov"; label9.stringValue = "Oct"; label10.stringValue = "Sept"; label11.stringValue = "Aug"; label12.stringValue = "July"; label13.stringValue = "July"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileR"); self.imageView2.image = #imageLiteral(resourceName: "tileRRB"); self.imageView3.image = #imageLiteral(resourceName: "tileRB"); self.imageView4.image = #imageLiteral(resourceName: "tileBR"); self.imageView5.image = #imageLiteral(resourceName: "tileBBR"); self.imageView6.image = #imageLiteral(resourceName: "tileB"); imageView7.image = #imageLiteral(resourceName: "tileB"); self.imageView8.image = #imageLiteral(resourceName: "tileBBR"); self.imageView9.image = #imageLiteral(resourceName: "tileBR"); self.imageView10.image = #imageLiteral(resourceName: "tileRB"); self.imageview11.image = #imageLiteral(resourceName: "tileRRB");  self.imageView12.image = #imageLiteral(resourceName: "tileR");  self.imageView13.image = #imageLiteral(resourceName: "tileR")
            
        case  8:
            print(8)
            label1.stringValue = "July" ; label2.stringValue = "June"; label3.stringValue = "May"; label4.stringValue = "April"; label5.stringValue = "March"; label6.stringValue = "Feb"; label7.stringValue = "Jan"; label8.stringValue = "Dec"; label9.stringValue = "Nov"; label10.stringValue = "Oct"; label11.stringValue = "Sept"; label12.stringValue = "Aug"; label13.stringValue = "Aug"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileR"); self.imageView2.image = #imageLiteral(resourceName: "tileR"); self.imageView3.image = #imageLiteral(resourceName: "tileRRB"); self.imageView4.image = #imageLiteral(resourceName: "tileRB"); self.imageView5.image = #imageLiteral(resourceName: "tileBR"); self.imageView6.image = #imageLiteral(resourceName: "tileBBR"); imageView7.image = #imageLiteral(resourceName: "tileB"); self.imageView8.image = #imageLiteral(resourceName: "tileB"); self.imageView9.image = #imageLiteral(resourceName: "tileBBR"); self.imageView10.image = #imageLiteral(resourceName: "tileBR"); self.imageview11.image = #imageLiteral(resourceName: "tileRB");  self.imageView12.image = #imageLiteral(resourceName: "tileRRB");  self.imageView13.image = #imageLiteral(resourceName: "tileRRB")
            
        case  9:
            print(9)
            label1.stringValue = "Aug" ; label2.stringValue = "July"; label3.stringValue = "June"; label4.stringValue = "May"; label5.stringValue = "April"; label6.stringValue = "March"; label7.stringValue = "Feb"; label8.stringValue = "Jan"; label9.stringValue = "Dec"; label10.stringValue = "Nov"; label11.stringValue = "Oct"; label12.stringValue = "Sept"; label13.stringValue = "Sept"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileRRB"); self.imageView2.image = #imageLiteral(resourceName: "tileR"); self.imageView3.image = #imageLiteral(resourceName: "tileR"); self.imageView4.image = #imageLiteral(resourceName: "tileRRB"); self.imageView5.image = #imageLiteral(resourceName: "tileRB"); self.imageView6.image = #imageLiteral(resourceName: "tileBR"); imageView7.image = #imageLiteral(resourceName: "tileBBR"); self.imageView8.image = #imageLiteral(resourceName: "tileB"); self.imageView9.image = #imageLiteral(resourceName: "tileB"); self.imageView10.image = #imageLiteral(resourceName: "tileBBR"); self.imageview11.image = #imageLiteral(resourceName: "tileBR");  self.imageView12.image = #imageLiteral(resourceName: "tileRB");  self.imageView13.image = #imageLiteral(resourceName: "tileRB")
            
        case  10:
            print(10)
            label1.stringValue = "Sept" ; label2.stringValue = "Aug"; label3.stringValue = "July"; label4.stringValue = "June"; label5.stringValue = "May"; label6.stringValue = "April"; label7.stringValue = "March"; label8.stringValue = "Feb"; label9.stringValue = "Jan"; label10.stringValue = "Dec"; label11.stringValue = "Nov"; label12.stringValue = "Oct"; label13.stringValue = "Oct"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileRB"); self.imageView2.image = #imageLiteral(resourceName: "tileRRB"); self.imageView3.image = #imageLiteral(resourceName: "tileR"); self.imageView4.image = #imageLiteral(resourceName: "tileR"); self.imageView5.image = #imageLiteral(resourceName: "tileRRB"); self.imageView6.image = #imageLiteral(resourceName: "tileRB"); imageView7.image = #imageLiteral(resourceName: "tileBR"); self.imageView8.image = #imageLiteral(resourceName: "tileBBR"); self.imageView9.image = #imageLiteral(resourceName: "tileB"); self.imageView10.image = #imageLiteral(resourceName: "tileB"); self.imageview11.image = #imageLiteral(resourceName: "tileBBR");  self.imageView12.image = #imageLiteral(resourceName: "tileBR");  self.imageView13.image = #imageLiteral(resourceName: "tileBR")
            
        case  11:
            print(11)
            label1.stringValue = "Oct" ; label2.stringValue = "Sept"; label3.stringValue = "Aug"; label4.stringValue = "July"; label5.stringValue = "June"; label6.stringValue = "May"; label7.stringValue = "April"; label8.stringValue = "March"; label9.stringValue = "Feb"; label10.stringValue = "Jan"; label11.stringValue = "Dec"; label12.stringValue = "Nov"; label13.stringValue = "Nov"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileBR"); self.imageView2.image = #imageLiteral(resourceName: "tileRB"); self.imageView3.image = #imageLiteral(resourceName: "tileRRB"); self.imageView4.image = #imageLiteral(resourceName: "tileR"); self.imageView5.image = #imageLiteral(resourceName: "tileR"); self.imageView6.image = #imageLiteral(resourceName: "tileRRB"); imageView7.image = #imageLiteral(resourceName: "tileRB"); self.imageView8.image = #imageLiteral(resourceName: "tileBR"); self.imageView9.image = #imageLiteral(resourceName: "tileBBR"); self.imageView10.image = #imageLiteral(resourceName: "tileB"); self.imageview11.image = #imageLiteral(resourceName: "tileB");  self.imageView12.image = #imageLiteral(resourceName: "tileBBR");  self.imageView13.image = #imageLiteral(resourceName: "tileBBR")
            
        case  12:
            print(12)
            label1.stringValue = "Nov" ; label2.stringValue = "Oct"; label3.stringValue = "Sept"; label4.stringValue = "Aug"; label5.stringValue = "July"; label6.stringValue = "June"; label7.stringValue = "May"; label8.stringValue = "April"; label9.stringValue = "March"; label10.stringValue = "Feb"; label11.stringValue = "Jan"; label12.stringValue = "Dec"; label13.stringValue = "Dec"
            
            self.imageView1.image = #imageLiteral(resourceName: "tileBBR"); self.imageView2.image = #imageLiteral(resourceName: "tileBR"); self.imageView3.image = #imageLiteral(resourceName: "tileRB"); self.imageView4.image = #imageLiteral(resourceName: "tileRRB"); self.imageView5.image = #imageLiteral(resourceName: "tileR"); self.imageView6.image = #imageLiteral(resourceName: "tileR"); imageView7.image = #imageLiteral(resourceName: "tileRRB"); self.imageView8.image = #imageLiteral(resourceName: "tileRB"); self.imageView9.image = #imageLiteral(resourceName: "tileBR"); self.imageView10.image = #imageLiteral(resourceName: "tileBBR"); self.imageview11.image = #imageLiteral(resourceName: "tileB");  self.imageView12.image = #imageLiteral(resourceName: "tileB");  self.imageView13.image = #imageLiteral(resourceName: "tileB")
            
        default: break
        }
    }
    
    var seedBoxItem: SeedBoxItem!
    var plantOutDate: NSDate!
    var harvestDate: NSDate!
    
    func selectedseedBoxItem(seedBoxItem: SeedBoxItem){
        self.seedBoxItem = seedBoxItem
        self.plantingDateRules()
        self.seedBoxItem.plantOutDate = self.plantOutDate
        self.seedBoxItem.harvestDate = self.harvestDate
    }
    
    func plantingDateRules(){
        let calendar = Calendar.current
        if Int(seedBoxItem.plantFromMonth) < (self.thisMonth){
            let plantOutDate = calendar.date(from: DateComponents(year: self.thisYear + 1 , month:Int(seedBoxItem.plantFromMonth) , day: 1))!
            self.plantOutDate = plantOutDate as NSDate!
        }
        else{
            let plantOutDate = calendar.date(from: DateComponents(year: self.thisYear  , month:Int(seedBoxItem.plantFromMonth) , day: 1))!
            self.plantOutDate = plantOutDate as NSDate!
        }
        if (seedBoxItem.harvestFromMonth < seedBoxItem.plantFromMonth){
            let year = calendar.component(.year, from: self.plantOutDate as Date)
            let harvestDate = calendar.date(from: DateComponents(year: year + 1 , month:Int(seedBoxItem.harvestFromMonth) , day: 1))!
            self.harvestDate = harvestDate as NSDate!
        }
        else{
            let year = calendar.component(.year, from: self.plantOutDate as Date)
            let harvestDate = calendar.date(from: DateComponents(year: year  , month:Int(seedBoxItem.harvestFromMonth) , day: 1))!
            self.harvestDate = harvestDate as NSDate!
        }
    }
    
    var formatter: DateFormatter!
    @IBOutlet weak var sliderTextField: NSTextField!
    var sliderString: String!
    var dateString: String!
    
    @IBAction func slider2(_ sender: NSSlider) {
        let a :Int = Int(sender.intValue)
        let anotherDate = Calendar.current.date(from: DateComponents(year: thisYear, month: thisMonth, day: thisDay + a))!
        let sliderString = self.formatter.string(from: anotherDate)
        self.sliderTextField.stringValue = sliderString
        self.delegate?.dateSelectionChanged(date: anotherDate as NSDate)
    }
    
    //forTesting only
    @IBAction func startTimer(_ sender: Any) {
        _ = Timer.scheduledTimer( timeInterval: 0.2, target:self, selector: #selector(self.start), userInfo: nil, repeats: true)
    }
    
    @IBOutlet weak var nextYearLabel: NSTextField!
    
    var a = 1
    
    func start(){
        a = a + 1
        let timedDate = Calendar.current.date(from: DateComponents(year: 2016, month: 11, day: a))!
        let calendar = Calendar.current
        let year = calendar.component(.year, from: timedDate)
        let month = calendar.component(.month, from: timedDate)
        let day = calendar.component(.day, from: timedDate)
        self.nextYearLabel.intValue = Int32(year)
        self.thisMonth = month
        self.updateTheLabels()
        self.leadingConstraint.constant = CGFloat(day * -2)
    }
    
    
}
