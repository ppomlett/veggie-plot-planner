//
//  SelectedSeedItemViewcontroller.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 24/11/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa

class SelectedSeedItemViewcontroller: NSViewController {

    var seedBoxItem: SeedBoxItem!
    
    @IBOutlet weak var seedItemImageView: NSImageView!
    @IBOutlet weak var seedItemNameLabel: NSTextField!
    @IBOutlet weak var seedItemDescriptionLabel: NSTextField!
    @IBOutlet weak var plantOutButton: NSButton!
    @IBOutlet weak var harvestButton: NSButton!
    @IBOutlet weak var plantOutLabel: NSTextField!
    @IBOutlet weak var harvestLabel: NSTextField!
    @IBOutlet weak var startLabel: NSTextField!
    @IBOutlet weak var datePickerTypeLabel: NSTextField!
    @IBOutlet weak var datePicker: NSDatePicker!
    
    
    @IBAction func plantOutAction(_ sender: NSButton) {
    }
    

    @IBAction func harvestAction(_ sender: Any) {
    }
    
    @IBAction func datePickerAction(_ sender: NSDatePicker) {
    }
    
    @IBAction func closeAction(_ sender: NSButton) {
        self.dismissViewController(self)
        
    }
    
    
    func updateUI() {
     self.seedItemImageView.image = NSImage(named: self.seedBoxItem.imageName!)
     self.seedItemNameLabel.stringValue = self.seedBoxItem.seedName!
     self.seedItemDescriptionLabel.stringValue = self.seedBoxItem.seedDescription!
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
    }
    
}
