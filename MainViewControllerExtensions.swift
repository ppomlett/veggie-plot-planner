//
//  MainViewControllerExtensions.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 26/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Cocoa


extension MainViewController: NSCollectionViewDelegate, NSCollectionViewDataSource, SeedBoxItemItemDelegate {
    //CollectionView for seedbox items
    // MARK: - Collection View Data Source / Delegate
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return  (seedBoxItems.fetchedObjects?.count)!
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt
        indexPath: IndexPath) -> NSCollectionViewItem {
        let item = self.collectionView.makeItem(withIdentifier: "item", for: indexPath) as! SeedBoxItemItem
        item.delegate = self
        let name = seedBoxItems.object(at: indexPath).seedName
        var description = seedBoxItems.object(at: indexPath).seedDescription
        if description == nil{
            description = ""
        }
        var imageName = seedBoxItems.object(at: indexPath).imageName
        if imageName == nil{
            imageName = "cab"
        }
        let image = NSImage(named:imageName! )
        
        //  item.imageView?.image = image
        //      item.vegImageView.image = image
        item.nameLabel.stringValue = name!
        item.descriptionLabel.stringValue = description!
        item.itemImageView.image = image
        return      item
    }
    
    // SeedBoxItemItemDelegate method
    //present  info view controller on collection view double click
    func doubleClickedOnItem(item: NSCollectionViewItem){
        //if info view is showing dismiss befor presenting from new collection item
        if((self.seedBoxItemInfoVC.presenting) != nil){
            self.dismissViewController(self.seedBoxItemInfoVC)
        }
        //present info view controller
        if (self.selectedSeedBoxItem != nil){
            self.seedBoxItemInfoVC.selectedSeedBoxItem = self.selectedSeedBoxItem
            self.presentViewController(self.seedBoxItemInfoVC, asPopoverRelativeTo: (item.view.bounds), of: (item.view), preferredEdge: NSRectEdge.minX , behavior: NSPopoverBehavior.transient)
        }
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        //  grap seedBoxItem object to pass to info view controller
        self.selectedSeedBoxItem = self.seedBoxItems.object(at: indexPaths.first!)
       
        self.timeLineViewController.updateUIForSelectedSeedItem(seedItem: self.selectedSeedBoxItem)
         self.bedController.selectedSeeBoxItem(seedBoxItem: self.selectedSeedBoxItem)
        
        print(selectedSeedBoxItem.harvestDate! as NSDate )
        
    }
    
    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
       
    }
    
    func collectionView(_ collectionView: NSCollectionView, pasteboardWriterForItemAt indexPath: IndexPath) -> NSPasteboardWriting? {
        let image =   "o"         //#imageLiteral(resourceName: "cab.png")           //self.seedBoxItems[indexPath.item]
        return image as NSPasteboardWriting?
    }
    
    func collectionView(_ collectionView: NSCollectionView, canDragItemsAt indexes: IndexSet, with event: NSEvent) -> Bool {
        
        return true
    }
    
    // MARK: - NSFetchedResultsController Delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
        
        self.collectionView.reloadData()
    }
   
}


















