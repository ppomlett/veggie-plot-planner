//
//  Note+CoreDataProperties.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 25/10/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData
//import

extension Note {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Note> {
        return NSFetchRequest<Note>(entityName: "Note");
    }

    @NSManaged public var noteContent: String?
    @NSManaged public var noteDate: NSDate?
    @NSManaged public var noteName: String?
    @NSManaged public var notesBed: Bed?
    @NSManaged public var notesPlant: Plant?
    @NSManaged public var notesPlot: Plot?

}
