//
//  Plant+CoreDataProperties.swift
//  Veggie Plot Planner
//
//  Created by Peter Pomlett on 03/11/2016.
//  Copyright © 2016 Peter Pomlett. All rights reserved.
//

import Foundation
import CoreData


extension Plant {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Plant> {
        return NSFetchRequest<Plant>(entityName: "Plant");
    }

    @NSManaged public var imageName: String?
    @NSManaged public var isPerennial: Bool
    @NSManaged public var isPlanted: Bool
    @NSManaged public var isRemoved: Bool
    @NSManaged public var name: String?
    @NSManaged public var plantDescription: String?
    @NSManaged public var plantedDate: NSDate?
    @NSManaged public var plantHeight: Float
    @NSManaged public var plantWidth: Float
    @NSManaged public var removedDate: NSDate?
    @NSManaged public var viewX: Float
    @NSManaged public var viewY: Float
    @NSManaged public var viewHeight: Float
    @NSManaged public var viewWidth: Float
    @NSManaged public var plantBed: Bed?
    @NSManaged public var plantNotes: NSSet?
    @NSManaged public var plantToDos: NSSet?

}

// MARK: Generated accessors for plantNotes
extension Plant {

    @objc(addPlantNotesObject:)
    @NSManaged public func addToPlantNotes(_ value: Note)

    @objc(removePlantNotesObject:)
    @NSManaged public func removeFromPlantNotes(_ value: Note)

    @objc(addPlantNotes:)
    @NSManaged public func addToPlantNotes(_ values: NSSet)

    @objc(removePlantNotes:)
    @NSManaged public func removeFromPlantNotes(_ values: NSSet)

}

// MARK: Generated accessors for plantToDos
extension Plant {

    @objc(addPlantToDosObject:)
    @NSManaged public func addToPlantToDos(_ value: ToDo)

    @objc(removePlantToDosObject:)
    @NSManaged public func removeFromPlantToDos(_ value: ToDo)

    @objc(addPlantToDos:)
    @NSManaged public func addToPlantToDos(_ values: NSSet)

    @objc(removePlantToDos:)
    @NSManaged public func removeFromPlantToDos(_ values: NSSet)

}
